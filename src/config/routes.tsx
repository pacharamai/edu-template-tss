// import { Redirect } from 'react-router-dom'
import Dashboard from '../pages/Dashboard'
import AppLayout from '../layout/AppLayout'
import tableRoute from '../pages/Table/route'
import editorRoute from '../pages/Editor/route'
import formRoute from '../pages/Form/route'


const routes: {
  component: any;
  path: string[];
  routes: ({
    menu: string;
    path: string;
    component: any;
  }
    | {
      menu: string;
      path: string;
      exact: true;
      component: any;
    })[];
}[] = [
    {
      component: AppLayout,
      path: [],
      routes: [
        {
          menu: 'Dashboard',
          path: '/',
          exact: true,
          component: Dashboard
        },
        ...tableRoute,
        ...editorRoute,
        ...formRoute
      ]
    }
  ]
// export default routes;

const formattedRoute = routes.map(route => {
  if (route.routes) {
    route.path = route.routes.map(r => r.path)
  }
  return route
})

export default formattedRoute