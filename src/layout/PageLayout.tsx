import React from 'react'
import { PageHeader } from 'antd'

type PageLayoutProps = {
  title: string;
  subTitle: string;
  children: any | null | undefined | '';
}

const PageLayout: React.FC<PageLayoutProps> = (props: PageLayoutProps) => {
  const { title, subTitle, children } = props
  return (
    <PageHeader
      // onBack={() => window.history.back()}
      title={title}
      subTitle={subTitle}
      // breadcrumb={{routes}}
      style={{ backgroundColor: 'white' }}
    >
      {children}
    </PageHeader>
  )
}

export default PageLayout
