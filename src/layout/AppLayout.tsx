import React from 'react'
import { Layout } from 'antd'
import AppSidebar from './AppSidebar';
import AppHeader from './AppHeader';
import AppContent from './AppContent';

type Route = {
  menu: string;
  path: string;
  routes: any[];
}

type AppLayoutProps = {
  route: Route;
}

const AppLayout: React.FC<AppLayoutProps> = (props: AppLayoutProps) => {
  const { route } = props
  return (
    <Layout style={{ overflowX: 'hidden', maxHeight: '100vh', minHeight: '100vh' }}>
      <AppHeader />
      <Layout style={{ minWidth: 320 }}>
        <AppSidebar route={route} />
        <AppContent route={route} />
      </Layout>
    </Layout>
  )
}

export default AppLayout
