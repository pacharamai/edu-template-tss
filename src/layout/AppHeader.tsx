
import React from 'react'
import { Layout, Row, Col, Avatar, Menu, List } from 'antd'
import { BellOutlined } from '@ant-design/icons'
import { Link } from 'react-router-dom'
const {
  Header,
} = Layout


const AppHeader: React.FC = () => {
  const data = [
    {
      title: 'Notification 1',
    },
    {
      title: 'Notification 1',
    }
  ]
  return (
    <Header style={{ padding: 0 }}>
      <Row justify='space-between' style={{ display: 'flex' }}>
        <div className='logo'>
          Education Website
        </div>
        <Col>
          <Menu
            // theme='light'
            mode='horizontal'
            style={{ lineHeight: '64px' }}
          >
            <Menu.SubMenu title={(
              <BellOutlined style={{ fontSize: '18px' }} />
            )}>t
              <div style={{ maxWidth: 320 }}>
                <List
                  bordered
                  itemLayout='horizontal'
                  dataSource={data}
                  renderItem={item => void (
                    <List.Item>
                      <List.Item.Meta
                        title={<a href='/'>{item.title}</a>}
                        description={item.title}
                      />
                    </List.Item>
                  )}
                />
              </div>
            </Menu.SubMenu>
            <Menu.SubMenu title={(
              <div className='user-profile'>
                <Avatar shape='square'>
                  T
                </Avatar>
                <div className='user-detail'>
                  <span>test test</span>
                </div>
              </div>
            )}>
              <Menu.Item>
                <Link to='/'>
                  My profile
                </Link>
              </Menu.Item>
              <Menu.Item>
                Logout
              </Menu.Item>
            </Menu.SubMenu>
          </Menu>
        </Col>
      </Row>
    </Header>
  )
}

export default AppHeader
