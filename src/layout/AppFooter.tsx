import React from 'react'
import { Layout } from 'antd'
const {
  Footer,
} = Layout
const AppFooter: React.FC = () => {
  return (
    <Footer>
      {/* © copyright */}
    </Footer>
  )
}

export default AppFooter
