import React from 'react'
import { Layout, Menu } from 'antd'
import {
  useHistory
} from "react-router-dom";
const {
  Sider,
} = Layout

type Route = {
  menu: string;
  path: string;
  routes: any[];
}

type AppSidebarProps = {
  route: Route;
}

const AppSidebar: React.FC<AppSidebarProps> = (props: AppSidebarProps) => {
  const { route } = props
  // const [currentPath, setcurrentPath] = useState()
  // const location = useLocation();
  const history = useHistory();
  // React.useEffect(() => {
  //   debugger
  //   console.log(history)
  //   setcurrentPath(location.pathname)
  // }, [location, history]);
  const myRoutes = route.routes
  const renderMenu: React.FC<Route> = (r: Route) => {
    if (!r.menu) {
      return null
    }
    if (r.routes) {
      return (
        <Menu.SubMenu key={r.path} title={r.menu}>
          {r.routes.map(renderMenu)}
        </Menu.SubMenu>
      )
    }
    return (
      <Menu.Item key={r.path} onClick={(): void => history.push(r.path)}>
        <span className="nav-text">{r.menu}</span>
      </Menu.Item>
    )
  }
  return (
    <Sider
      breakpoint="lg"
      collapsedWidth="0"
      theme="light"
    >
      <Menu mode="inline" defaultSelectedKeys={['/']}>
        {myRoutes.map((route) => (
          renderMenu(route)
        ))}
      </Menu>
    </Sider>
  )
}

export default AppSidebar
