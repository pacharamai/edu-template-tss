import FormDemo from './Form'

const basePath = '/form'
const formRoute = [
  {
    menu: 'Form',
    path: basePath,
    component: FormDemo,
  },
]

export default formRoute