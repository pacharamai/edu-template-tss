import React, { useEffect } from 'react'
import { Input, Button, Form } from 'antd'
import { useForm, Controller } from 'react-hook-form';
import PageLayout from '../../layout/PageLayout'


const FormDemo: React.FC = () => {
  const { register, handleSubmit, setValue, control, errors } = useForm();
  const onSubmit: Function = (data: any) => console.log(data);
  const handleChange: Function = (e: any) => {
    setValue([e.target.name], e.target.value);
  }

  useEffect(() => {
    register({ name: 'name' }); // custom register react-select 
    register({ name: 'surname' }); // custom register antd input
  }, [register])

  return (
    <PageLayout title='Form With React hook form' subTitle=''>
      <form onSubmit={handleSubmit((data) => onSubmit(data))}>
        <Form.Item label="Name">
          <Input name='name' onChange={(e) => handleChange(e)} />
        </Form.Item>
        <Form.Item label="Surname">
          <Input name='surname' onChange={(e) => handleChange(e)} />
        </Form.Item>
        <Form.Item label="Ant Control">
          <Controller as={Input} name='antcontrol' control={control} defaultValue="" rules={{ required: true }} />
          {
            errors.antcontrol && <span>antconrol require</span>
          }
        </Form.Item>
        <Button htmlType='submit'>submit</Button>
      </form>
    </PageLayout>
  )
}

export default FormDemo