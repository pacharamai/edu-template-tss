import React from 'react'
import PageLayout from '../layout/PageLayout'

const Dashboard: React.FC = () => {
  return (
    <PageLayout title="Dashboard" subTitle=''>
      <p>Dashboard</p>
    </PageLayout>
  )
}

export default Dashboard
