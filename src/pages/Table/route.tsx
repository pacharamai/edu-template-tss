import Tab from './Tab'

const basePath = '/table'
const tableRoute = [
  {
    menu: 'Table',
    path: basePath,
    component: Tab,
  },
]

export default tableRoute