import React, { useState } from 'react'
import PageLayout from '../../layout/PageLayout'
import { Table, Button } from 'antd'
import { FolderViewOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons'


const List: React.FC = () => {
  const defaultPage = 1
  const defaultPageSize = 5
  const defaultSelectRowKeys: any[] = []
  const mockData = [
    {
      name: "ชื่อผู้ป่วย - นามสกุล 1",
      type: "ประวัตการรักษา 1",
      id: 1
    },
    {
      name: "ชื่อผู้ป่วย - นามสกุล 2",
      type: "ประวัตการรักษา 2",
      id: 2
    },
    {
      name: "ชื่อผู้ป่วย - นามสกุล 3",
      type: "ประวัตการรักษา 3",
      id: 3
    },
    {
      name: "ชื่อผู้ป่วย - นามสกุล 4",
      type: "ประวัตการรักษา 4",
      id: 4
    },
    {
      name: "ชื่อผู้ป่วย - นามสกุล 5",
      type: "ประวัตการรักษา 5",
      id: 5
    },
    {
      name: "ชื่อผู้ป่วย - นามสกุล 6",
      type: "ประวัตการรักษา 6",
      id: 6
    },
    {
      name: "ชื่อผู้ป่วย - นามสกุล 7",
      type: "ประวัตการรักษา 7",
      id: 7
    },
    {
      name: "ชื่อผู้ป่วย - นามสกุล 8",
      type: "ประวัตการรักษา 8",
      id: 8
    },
    {
      name: "ชื่อผู้ป่วย - นามสกุล 9",
      type: "ประวัตการรักษา 9",
      id: 9
    },
    {
      name: "ชื่อผู้ป่วย - นามสกุล 10",
      type: "ประวัตการรักษา 10",
      id: 10
    },
  ]

  const [page, setpage] = useState(defaultPage)
  const [pageSize, setpageSize] = useState(defaultPageSize)
  const [selectedRowKeys, setSelectedRowKeys] = useState(defaultSelectRowKeys)

  const handleChange = (pagination: any, filters: any, sorter: any) => {
    setpage(pagination.current)
    setpageSize(pagination.pageSize)
  }
  const columns = [
    {
      title: 'ชื่อ-นามสกุล',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'ประวัติการรักษา',
      dataIndex: 'type',
      key: 'type',
    },
    {
      title: 'เครื่องมือ',
      key: 'operation',
      render: (row: any) => void (<>
        <Button><FolderViewOutlined /></Button>
        <Button><EditOutlined /></Button>
        <Button><DeleteOutlined /></Button>
      </>
      )
    },
    {
      title: 'สถานะ',
      key: 'status',
      render: (row: any) => void (<Button>มอบหมายงาน</Button>)
    }
  ];
  const onSelectChange = (selectedRowKeys: any[]) => {
    setSelectedRowKeys(selectedRowKeys)
  }
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  }
  return (
    <>
      <div className='tab-title-group'>
        <div className='title'>
          <h2>{`ผู้ป่วยทั้งหมด (${mockData.length})`}</h2>
          <div className='button-group'>
            <Button>ผู้ป่วย Ward 1</Button>
            <Button>ผู้ป่วย Ward 2</Button>
            <Button>ผู้ป่วย Ward 3</Button>
            <Button>ผู้ป่วย Ward 4</Button>
          </div>
        </div>
        <div className='button-group'>
          <Button>กรอง</Button>
        </div>
      </div>
      <PageLayout
        title=''
        subTitle=''
      >
        <Table rowKey="id" rowSelection={rowSelection} onChange={handleChange} columns={columns} dataSource={mockData} pagination={{
          defaultCurrent: defaultPageSize,
          defaultPageSize: defaultPageSize,
          current: page,
          pageSize: pageSize,
          total: mockData.length
        }} />
      </PageLayout>
    </>
  )
}

export default List
