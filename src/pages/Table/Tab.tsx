import React from 'react'
import { Tabs } from 'antd'

import List from './List'
import PageLayout from '../../layout/PageLayout'

const { TabPane } = Tabs

const Tab: React.FC = () => (
  <Tabs defaultActiveKey='1'>
    <TabPane tab='ผู้ป่วย' key='1'>
      <List />
    </TabPane>
    <TabPane tab='นักศึกษาแพทย์' key='2'>
      <PageLayout title='นักศึกษาแพทย์' subTitle='' >
      </PageLayout>
    </TabPane>
  </Tabs>
)
export default Tab