import Editor from './Editor'

const basePath = '/editor'
const editorRoute = [
  {
    menu: 'Editor',
    path: basePath,
    component: Editor,
  },
]

export default editorRoute